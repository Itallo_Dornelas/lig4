let animacaoGif = document.getElementById('animacaoGif');
let regras = document.getElementById('regras')
let regras1 = document.getElementById('regras1')
let home = document.getElementById('home')
let playMusic=document.getElementById('playMusic')
let stopMusic=document.getElementById('stopMusic')
// let jogar=document.getElementById('jogar')

regras1.addEventListener("click", aparecerTexto)
home.addEventListener("click",aparecerGif)
playMusic.addEventListener("click", play_single_sound)
stopMusic.addEventListener("click", pause_single_sound)
// jogar.addEventListener("click", play_single_sound)

function aparecerTexto() {
    regras.style.display = "flex";
    animacaoGif.style.display = "none";
}

function aparecerGif() {
regras.style.display = "none";
animacaoGif.style.display ="flex";
}

function play_single_sound() {
    document.getElementById('player').play();
}

function pause_single_sound() {
    document.getElementById('player').pause();
}