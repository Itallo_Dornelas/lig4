const tabuleiro = [
  [0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0],
];



/* Criar tabuleiro  */
let tabuleiroNovo = [...tabuleiro];
let linhaTabuleiro;
let disco;
let criarColuna;
let criarCelula;
let jogadorAtual = 1;
let discoJogador1 = 1;
let discoJogador2 = 2;
let arrayGanhador = [];
let vitoriasJogador1 = 0;
let vitoriasJogador2 = 0;
let bolaSelecionada = [];
let posicaoBolaSelecionadaNoTabuleiro = [];

document.getElementById(`jogador1`).className = `vezDoJogador`;

function criarTabuleiro() {
  for (let i = 0; i <= tabuleiroNovo.length; i++) {
    criarColuna = document.createElement(`div`);
    criarColuna.id = `colunaTabuleiro${i}`;
    criarColuna.className = `colunasTabuleiro`;
    document.getElementById(`tabuleiro`).appendChild(criarColuna);
  }
  for (let n = 0; n < tabuleiroNovo.length; n++) {
    linhaTabuleiro = tabuleiroNovo[n];
    for (let j = 0; j <= tabuleiroNovo.length; j++) {
      disco = linhaTabuleiro[j];
      posicaoBolaSelecionadaNoTabuleiro = [];
      posicaoBolaSelecionadaNoTabuleiro.push(n);
      posicaoBolaSelecionadaNoTabuleiro.push(j);
      criarCelula = document.createElement(`div`);
      criarCelula.innerHTML = ``;
      criarCelula.className = `celulasTabuleiro`;
      adicionarClasseBolaSelecionada();
      checarBolasVencedoras(arrayGanhador, [n, j]);

      if (disco == 0) criarCelula.className += ` bolaOriginal`;
      if (disco == 1) criarCelula.className += ` ${times[clubeJogador1]}`;
      // document.querySelector(`.bolaJogador1`).style.backgroundImage = `url(img/Goias_logo.png)`
      if (disco == 2) criarCelula.className += ` ${times[clubeJogador2]}`;
      //document.querySelector(`.bolaJogador2`).style.backgroundImage = `url(img/Internacional_logo.png)`
      document.getElementById(`colunaTabuleiro${j}`).appendChild(criarCelula);
    }
  }
  /// document.querySelector(`.bolaJogador1`).style.backgroundImage = `url(img/Goias_logo.png)`}
  //document.querySelector(`.bolaJogador2`).style.backgroundImage = `url(img/Internacional_logo.png)`

  document.getElementById(`jogador1`).className += ` ${times[clubeJogador1]}`;
  document.getElementById(`jogador2`).className += ` ${times[clubeJogador2]}`;
  receberClicks();
}
criarTabuleiro();
function adicionarClasseBolaSelecionada() {
  if (
    bolaSelecionada[0] == posicaoBolaSelecionadaNoTabuleiro[0] &&
    bolaSelecionada[1] == posicaoBolaSelecionadaNoTabuleiro[1]
  )
    criarCelula.className += ` bolaJogada`;
}
/* Criar tabuleiro */
/* Criar o evento de seleção da jogada */
function receberClicks() {
  document
    .getElementById(`colunaTabuleiro0`)
    .addEventListener(`click`, function () {
      registrarJogada(0);
    });
  document
    .getElementById(`colunaTabuleiro1`)
    .addEventListener(`click`, function () {
      registrarJogada(1);
    });
  document
    .getElementById(`colunaTabuleiro2`)
    .addEventListener(`click`, function () {
      registrarJogada(2);
    });
  document
    .getElementById(`colunaTabuleiro3`)
    .addEventListener(`click`, function () {
      registrarJogada(3);
    });
  document
    .getElementById(`colunaTabuleiro4`)
    .addEventListener(`click`, function () {
      registrarJogada(4);
    });
  document
    .getElementById(`colunaTabuleiro5`)
    .addEventListener(`click`, function () {
      registrarJogada(5);
    });
  document
    .getElementById(`colunaTabuleiro6`)
    .addEventListener(`click`, function () {
      registrarJogada(6);
    });
}
function pegarColuna(colunaEscolhida) {
  let coluna = [];
  for (let i = 0; i < tabuleiroNovo.length; i++) {
    coluna.push(tabuleiroNovo[i][colunaEscolhida]);
  }
  return coluna;
}
//inserir as funções de verificação de vitória e empate na função de registrar jogada

function registrarJogada(colunaSelecionada) {
  document.getElementById(`tabuleiro`).innerHTML = ``;
  bolaSelecionada = [];
  let colunaAtual = colunaSelecionada;
  let arrayColuna = pegarColuna(colunaSelecionada);
  let posicaoDoDisco = arrayColuna.lastIndexOf(0);
  if (posicaoDoDisco == -1) return criarTabuleiro();
  document.getElementById(`jogador1`).className = ``;
  document.getElementById(`jogador2`).className = ``;
  document.getElementById(`jogador1`).className += ` ${times[clubeJogador1]}`;
  document.getElementById(`jogador2`).className += ` ${times[clubeJogador2]}`;
  if (jogadorAtual % 2 == 0) {
    jogadorAtual = discoJogador2;
    document.getElementById(`jogador1`).className = `vezDoJogador`;
  } else {
    jogadorAtual = discoJogador1;
    document.getElementById(`jogador2`).className = `vezDoJogador`;
  }
  tabuleiroNovo[posicaoDoDisco][colunaSelecionada] = jogadorAtual;
  bolaSelecionada.push(posicaoDoDisco);
  bolaSelecionada.push(colunaSelecionada);
  checarVitoria(colunaAtual);
  jogadorAtual++;
  criarTabuleiro();
}
function checarVitoria(colunaJogada) {
  if (vitoriaDiagonal() == true) {
    if (jogadorAtual == discoJogador1) vitoriasJogador1++;
    if (jogadorAtual == discoJogador2) vitoriasJogador2++;
    mensagemVitoria();
  }
  if (vitoriaHorizontal() == true) {
    if (jogadorAtual == discoJogador1) vitoriasJogador1++;
    if (jogadorAtual == discoJogador2) vitoriasJogador2++;
    mensagemVitoria();
  }

  if (vitoriaVertical(colunaJogada) == true) {
    if (jogadorAtual == discoJogador1) vitoriasJogador1++;
    if (jogadorAtual == discoJogador2) vitoriasJogador2++;
    mensagemVitoria();
  }
  if (verificarEmpate() == true) {
    if (jogadorAtual == discoJogador1) vitoriasJogador1++;
    if (jogadorAtual == discoJogador2) vitoriasJogador2++;
    mensagemEmpate();
  }
  document.getElementById(`placarJogador1`).innerHTML = vitoriasJogador1;
  document.getElementById(`placarJogador2`).innerHTML = vitoriasJogador2;
}

/* Criar o evento de seleção da jogada */

/* Revezar as vezes dos players*/

/* Revezar as vezes dos players*/

/*Mostrar aonde houve a vitoria*/
function checarBolasVencedoras(bolasVencedoras, bola) {
  for (var i = 0; i < bolasVencedoras.length; i++) {
    let contemBola = false;
    for (var j = 0; j < bolasVencedoras[i].length; j++) {
      if (bolasVencedoras[i][j] === bola[j]) {
        contemBola = true;
      } else {
        contemBola = false;
        break;
      }
    }
    if (contemBola) {
      criarCelula.className += ` bolaVencedora`;
      return true;
    }
  }
  return false;
}
/*Mostrar aonde houve a vitoria*/
/*Criar condição de vitoria Horizontal*/
function vitoriaHorizontal() {
  let vitoriaArr = [];
  let contador = 0;

  for (let i = 0; i < tabuleiroNovo.length; i++) {
    for (let j = 0; j < tabuleiroNovo[i].length; j++) {
      if (tabuleiroNovo[i][j] === jogadorAtual) {
        contador++;
        vitoriaArr.push([i, j]);
      } else {
        contador = 0;
        vitoriaArr = [];
      }
      if (contador === 4) {
        arrayGanhador = vitoriaArr;
        return true;
      }
    }
    contador = 0;
  }
  return false;
}

/*Criar condição de vitoria Horizontal*/

/*Criar condição de vitoria Vertical*/
function vitoriaVertical(colunaAtual) {
  let contador = 0;
  let vitoria = 0;
  let j = colunaAtual;
  let vitoriaArr = [];
  for (let i = 0; i < tabuleiroNovo.length; i++) {
    if (tabuleiroNovo[i][j] === jogadorAtual) {
      contador++;
      vitoriaArr.push([i, j]);
    } else {
      contador = 0;
      vitoriaArr = [];
    }
    if (contador === 4) {
      arrayGanhador = vitoriaArr;
      return true;
    }
  }
  return false;
}
/*Criar condição de vitoria Vertical*/

/*Criar condição de vitoria Diagonal descendente ou ascendente.*/
function vitoriaDiagonal() {
  //const n = 4

  const eixoX = tabuleiroNovo[0].length - 3;
  const eixoY = tabuleiroNovo.length - 3;

  for (let linha = 0; linha < eixoY; linha++) {
    for (let coluna = 0; coluna < eixoX; coluna++) {
      let celula = tabuleiroNovo[linha][coluna];

      if (celula !== 0) {
        if (
          celula === tabuleiroNovo[linha + 1][coluna + 1] &&
          celula === tabuleiroNovo[linha + 2][coluna + 2] &&
          celula === tabuleiroNovo[linha + 3][coluna + 3]
        ) {
          arrayGanhador.push([linha, coluna]);
          arrayGanhador.push([linha + 1, coluna + 1]);
          arrayGanhador.push([linha + 2, coluna + 2]);
          arrayGanhador.push([linha + 3, coluna + 3]);
          return true;
        }
      }
    }
  }

  for (let linha = 3; linha < tabuleiroNovo.length; linha++) {
    for (let coluna = 0; coluna < eixoX; coluna++) {
      let celula = tabuleiroNovo[linha][coluna];

      if (celula !== 0) {
        if (
          celula === tabuleiroNovo[linha - 1][coluna + 1] &&
          celula === tabuleiroNovo[linha - 2][coluna + 2] &&
          celula === tabuleiroNovo[linha - 3][coluna + 3]
        ) {
          arrayGanhador.push([linha, coluna]);
          arrayGanhador.push([linha - 1, coluna + 1]);
          arrayGanhador.push([linha - 2, coluna + 2]);
          arrayGanhador.push([linha - 3, coluna + 3]);

          return true;
        }
      }
    }
  }

  return false;
}
/*Criar condição de vitoria Diagonal descendente ou ascendente.*/

/*Criar condição de empate*/
function verificarEmpate() {
  let empate = false;
  for (let linha = 0; linha < tabuleiroNovo.length; linha++) {
    for (let coluna = 0; coluna < tabuleiroNovo[linha].length; coluna++) {
      let celula = tabuleiroNovo[linha][coluna];
      if (celula === 0) {
        return empate;
      }
    }
  }

  empate = true;

  return empate;
}
/*Criar condição de empate*/
