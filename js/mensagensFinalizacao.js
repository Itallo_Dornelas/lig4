let mensagemFinalizacao = document.getElementById("finalizacao");
document.getElementById("reset").addEventListener("click", reset);
document
  .getElementById("mudarTime")
  .addEventListener("click", trocarParaSelecionar);

function mensagemVitoria() {
  let pegarDiv = document.getElementById("vitoria");
  pegarDiv.style.display = `flex`;
  document.getElementById(
    "mensagemFinal"
  ).innerHTML = `<h2>Você venceu!!!</h2><img class="gif" src="http://cdn.lowgif.com/full/13f998d1c9d97fee-.gif" alt="gif win">`;
}

function mensagemEmpate() {
  let pegarDiv = document.getElementById("vitoria");
  pegarDiv.style.display = `flex`;
  document.getElementById("mensagemFinal").innerHTML =
    '<h2>Empatou!!!</h2> </br> </br>  <img class="gif" src="https://salveomaisquerido.files.wordpress.com/2012/11/38415400_1330657064.gif"  alt="gif win">';
}
const restart = document.getElementById("reload-pg");
restart.addEventListener("click", function () {
  location.reload();
});

function trocarParaSelecionar() {
  const pg3 = document.getElementById("pg3");
  const pg2 = document.getElementById("pg2");
  pg3.disabled = true;
  pg2.disabled = false;
  mudarClubes();
  reset();
}

function reset() {
  let pegarDiv = document.getElementById("vitoria");
  pegarDiv.style.display = `none`;
  tabuleiroNovo = [
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
  ];

  jogadorAtual = 1;
  arrayGanhador = [];
  document.getElementById(`tabuleiro`).innerHTML = ``;
  posicaoBolaSelecionadaNoTabuleiro = [];
  bolaSelecionada = [];

  return criarTabuleiro();
}
