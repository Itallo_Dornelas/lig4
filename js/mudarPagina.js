function User() {
  const nome1 = document.getElementById("nome1").value;
  const nome2 = document.getElementById("nome2").value;
  const pla = document.getElementById("nomeJogadores");
  const paragrafo1 = document.createElement("p");
  paragrafo1.className = "pgname";
  const paragrafo2 = document.createElement("p");
  paragrafo2.className = "pgname";
  paragrafo1.innerText = `${nome1}`;
  paragrafo2.innerText = `${nome2}`;
  pla.appendChild(paragrafo1);
  pla.appendChild(paragrafo2);
}

function trocarParaPaginaSelecionar() {
  const pg1 = document.getElementById("pg1");
  const pg2 = document.getElementById("pg2");
  pg1.disabled = true;
  pg2.disabled = false;
  User();
}

function trocarParaPaginaTabuleiro() {
  const pg1 = document.getElementById("pg1");
  const pg2 = document.getElementById("pg2");
  const pg3 = document.getElementById("pg3");
  pg1.disabled = true;
  pg2.disabled = true;
  pg3.disabled = false;
}

const mudarPgSelecionar = document.getElementById("jogar");
mudarPgSelecionar.addEventListener("click", trocarParaPaginaSelecionar);
