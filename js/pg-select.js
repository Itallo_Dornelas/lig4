const mainContainer = document.querySelector(".main-container");
const containerClubes = document.querySelector(".container-clubes");

const palmeiras = document.getElementById("container-palmeiras");
const corinthians = document.getElementById("container-corinthians");
const cruzeiro = document.getElementById("container-cruzeiro");
const goias = document.getElementById("container-goias");
const saopaulo = document.getElementById("container-saopaulo");
const internacional = document.getElementById("container-internacional");
const gremio = document.getElementById("container-gremio");
const vilaNova = document.getElementById("container-vilanova");
let times = {
  "container-goias": `bolaGoias`,
  "container-palmeiras": "bolaPalmeiras",
  "container-corinthians": `bolaCorinthians`,
  "container-cruzeiro": `bolaCruzeiro`,
  "container-saopaulo": `bolaSaopaulo`,
  "container-internacional": `bolaInternacional`,
  "container-gremio": `bolaGremio`,
  "container-vilanova": `bolaVilanova`,
};


const divPopUp = document.createElement("div");
divPopUp.classList.add("divPopUp");
containerClubes.appendChild(divPopUp);

const divConfirmacao = document.createElement("div");
divConfirmacao.classList.add("divConfirmacao");
divPopUp.appendChild(divConfirmacao);

const btnProsseguir = document.createElement("button");
btnProsseguir.classList.add(".btnProsseguir");
btnProsseguir.innerText = "Prosseguir";
divConfirmacao.appendChild(btnProsseguir);

const btnMudarClube = document.createElement("button");
btnMudarClube.classList.add(".btnMudarClube");
btnMudarClube.innerText = "Mudar Clube";
divConfirmacao.appendChild(btnMudarClube);

let clubeJogador1 = "";
let selection1;
let escudoJogador1;
let clubeJogador2 = "";
let selection2;
let escudoJogador2;
let primeiraEscolha = true;
let segundaEscolha = true;
const jogador1 = document.getElementById("jogador1");
function selecionarClube(event) {
  if (primeiraEscolha) {
    selection1 = event.currentTarget;
    selection1.classList.add("selection1");
    clubeJogador1 = selection1.id;
    escudoJogador1 = selection1.firstElementChild.src;
    nomeClubeJogador1 = selection1.firstElementChild.alt;
    document.getElementById(`jogador1`).className += ` ${times[clubeJogador1]}`;
   ;
    primeiraEscolha = false;
  } else if (primeiraEscolha === false && segundaEscolha === true) {
    selection2 = event.currentTarget;
    selection2.classList.add("selection2");
    clubeJogador2 = selection2.id;
    escudoJogador2 = selection2.firstElementChild.src;
    nomeClubeJogador2 = selection2.firstElementChild.alt;
    document.getElementById(`jogador2`).className += ` ${times[clubeJogador2]}`
    segundaEscolha = false;
    divPopUp.style.display = "flex";
  }
}
palmeiras.addEventListener("click", selecionarClube);
corinthians.addEventListener("click", selecionarClube);
cruzeiro.addEventListener("click", selecionarClube);
goias.addEventListener("click", selecionarClube);
saopaulo.addEventListener("click", selecionarClube);
internacional.addEventListener("click", selecionarClube);
gremio.addEventListener("click", selecionarClube);
vilaNova.addEventListener("click", selecionarClube);
function mudarClubes() {
  selection1.classList.remove("selection1");
  selection2.classList.remove("selection2");
  divPopUp.style.display = "none";
  primeiraEscolha = true;
  segundaEscolha = true;
}
function prosseguir() {
  trocarParaPaginaTabuleiro();
  selection1.classList.remove("selection1");
  selection2.classList.remove("selection2");
  primeiraEscolha = true;
  segundaEscolha = true;
}
btnMudarClube.addEventListener("click", mudarClubes);
btnProsseguir.addEventListener("click", prosseguir);
